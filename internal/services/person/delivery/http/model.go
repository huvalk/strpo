package http

import (
	"lab1/internal/services/person/repository"
)

type AddResponse struct {
	ID int64
}

type ViewResponse struct {
	Persons []repository.Person
}
