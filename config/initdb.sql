create table if not exists persons
	(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    firstName TEXT NOT NULL,
	lastName TEXT NOT NULL,
	yearOfBirth INTEGER NOT NULL);
