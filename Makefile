TARGET = bin/
COMPILE = go build
SRC = cmd/person/main.go

.PHONY: all clean build install uninstall

all: $(TARGET)

clean:
	rm -rf $(TARGET)

build:
	$(COMPILE) -o $(TARGET)l1 $(SRC)
	mkdir $(TARGET)data $(TARGET)config
	touch $(TARGET)data/sqlite.db
	cp config/initdb.sql $(TARGET)config/

uninstall:
	rm -rf /usr/local/bin/hw

test-cover:
	go test ./... -v -coverpkg=./internal/... | grep -v "no test files"
