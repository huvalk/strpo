package usecase

import (
	"lab1/internal/services/person/repository"
	"lab1/internal/services/person/repository/sqlite"
)

type PersonUseCase struct {
	r *sqlite.PersonRepository
}

func NewPersonUseCase(r *sqlite.PersonRepository) *PersonUseCase {
	return &PersonUseCase{r: r}
}

func (u *PersonUseCase) Add(p *repository.Person) (int64, error) {
	if p == nil {
		return 0, nil
	}
	if err := p.IsValid(); err != nil {
		return 0, err
	}

	return u.r.Add(p)
}

func (u *PersonUseCase) Remove(id int64) error {
	return u.r.Remove(id)
}

func (u *PersonUseCase) Update(id int64, p *repository.Person) error {
	if p == nil || id == 0 {
		return nil
	}
	if err := p.IsValid(); err != nil {
		return err
	}

	return u.r.Update(id, p)
}

func (u *PersonUseCase) View() ([]repository.Person, error) {
	return u.r.View()
}

func (u *PersonUseCase) Clean() error {
	return u.r.Clean()
}
