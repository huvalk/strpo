package test

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"lab1/internal/services/person/repository"
	"lab1/internal/services/person/server"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var db *sql.DB
var mock sqlmock.Sqlmock
var e *echo.Echo

func TestMain(m *testing.M) {
	var err error
	db, mock, err = sqlmock.New()
	if err != nil {
		os.Exit(1)
	}

	e = echo.New()
	server.NewServer(e, db)

	os.Exit(m.Run())
}

func TestAdd(t *testing.T) {
	row := mock.NewRows([]string{"id"}).AddRow(1)

	mock.ExpectQuery("INSERT INTO persons ").
		WithArgs("FirstName", "LastName", uint16(2001)).
		WillReturnRows(row)

	r, _ := http.NewRequest("POST", "/person", bytes.NewReader([]byte(
		"{\"ID\":0,\"FirstName\":\"FirstName\",\"LastName\":\"LastName\",\"YearOfBirth\":2001}",
	)))
	w := httptest.NewRecorder()

	e.ServeHTTP(w, r)

	assert.Equal(t, 201, w.Code, "Неверный статус ответа при создании")
}

func TestRemove(t *testing.T) {
	mock.ExpectExec("DELETE FROM persons").
		WithArgs(1).
		WillReturnResult(sqlmock.NewResult(1, 1))

	r, _ := http.NewRequest("DELETE", "/person/1", nil)
	w := httptest.NewRecorder()

	e.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code, "Неверный статус ответа при создании")
}

func TestUpdate(t *testing.T) {
	mock.ExpectExec("UPDATE persons ").
		WithArgs(int64(1), "FirstName", "LastName", uint16(2001)).
		WillReturnResult(sqlmock.NewResult(1, 1))

	r, _ := http.NewRequest("PUT", "/person/1", bytes.NewReader([]byte(
		"{\"ID\":1,\"FirstName\":\"FirstName\",\"LastName\":\"LastName\",\"YearOfBirth\":2001}",
	)))
	w := httptest.NewRecorder()

	e.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code, "Неверный статус ответа при создании")
}

func TestView(t *testing.T) {
	row := mock.NewRows([]string{"id", "firstName", "lastName", "yearOfBirth"}).
		AddRow(1, "FirstName", "LastName", uint16(2001))

	mock.ExpectQuery("SELECT").
		WillReturnRows(row)

	r, _ := http.NewRequest("GET", "/person", nil)
	w := httptest.NewRecorder()

	e.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code, "Неверный статус ответа при создании")
	bytes.NewReader([]byte(
		"{\"ID\":0,\"FirstName\":\"FirstName\",\"LastName\":\"LastName\",\"YearOfBirth\":2001}",
	))
	p := &repository.Person{}
	err := json.NewDecoder(w.Body).Decode(p)
	assert.NoError(t, err, "Неверно возвращен объект")
	assert.ObjectsAreEqual(*p, repository.Person{
		ID:          1,
		FirstName:   "FirstName",
		LastName:    "LastName",
		YearOfBirth: 2001,
	})
}

func TestClean(t *testing.T) {
	mock.ExpectExec("DELETE FROM persons ").
		WillReturnResult(sqlmock.NewResult(1, 1))

	r, _ := http.NewRequest("DELETE", "/person", nil)
	w := httptest.NewRecorder()

	e.ServeHTTP(w, r)

	assert.Equal(t, 200, w.Code, "Неверный статус ответа при создании")
}
