package server

import (
	"database/sql"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"lab1/internal/services/person/delivery/http"
	"lab1/internal/services/person/repository/sqlite"
	"lab1/internal/services/person/usecase"
)

type Server struct {
	port string
	e    *echo.Echo
}

func NewServer(e *echo.Echo, db *sql.DB) *Server {
	//middleware
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"http://localhost:8080"},
		AllowMethods:     []string{"GET", "HEAD", "PUT", "POST", "DELETE"},
		AllowCredentials: true,
	}))

	//person
	persons := sqlite.NewPersonRepository(db)
	person := usecase.NewPersonUseCase(persons)
	_ = http.NewPersonHandler(e, person)

	return &Server{
		port: ":8080",
		e:    e,
	}
}

func (s Server) ListenAndServe() error {
	return s.e.Start(s.port)
}

func (s Server) ListenAndServeTLS(sslPath string) error {
	return s.e.StartTLS(s.port, sslPath+"/fullchain.pem", sslPath+"/privkey.pem")
}
