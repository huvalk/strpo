package main

import (
	"github.com/kataras/golog"
	"github.com/labstack/echo"
	"lab1/internal/services/person/server"
	dbUtil "lab1/pkg/utils/db"
	"os"
)

func main() {
	db, err := dbUtil.NewDataBase()
	if err != nil {
		golog.Error("DB error: ", err.Error())
		os.Exit(1)
	}
	err = db.Ping()
	if err != nil {
		golog.Error("DB: ", err.Error())
		os.Exit(1)
	}

	e := echo.New()

	s := server.NewServer(e, db)

	golog.Fatal(s.ListenAndServe())
}
