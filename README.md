<div align="center">
Министерство науки и высшего образования Российской Федерации <br />
Федеральное государственное бюджетное образовательное учреждение <br />
высшего образования <br />
«Московский государственный технический университет <br />
имени Н.Э. Баумана <br />
(национальный исследовательский университет)» <br />
(МГТУ им. Н.Э. Баумана)
</div>
<hr />
<div align="center">
ФАКУЛЬТЕТ ИНФОРМАТИКА И СИСТЕМЫ УПРАВЛЕНИЯ <br />
КАФЕДРА КОМПЬЮТЕРНЫЕ СИСТЕМЫ И СЕТИ (ИУ6)
</div>
<br />
<div align="center">
ОТЧЕТ <br />
к лабораторной работе №1 <br />
по дисциплине "Современные технологии разработки <br />
программного обеспечения" <br />
Реализация приложения в многоуровневой архитектуре <br />
с использованием удалённого репозитория кода
</div>
<br />

Преподаватель: Фетисов М.В.

Студент группы ИУ6-23м Иванов Иван Иванович.

## Описание задания

Задача № 0: "Мои друзья".

Вариант задания № 0: Постройте диаграмму последовательности выполнения команды load. Диаграмма должна показывать прохождение выполнения команды между программными объектами, расположенными в соответствующих слоях многоуровневой архитектуры.

## Адрес проекта

Проект хранится в удаленном репозитории по адресу: [https://bmstu.codes/msdtm/labs/l1](https://bmstu.codes/msdtm/labs/l1).

## Диаграммы классов

Диаграммы классов создаются автоматически при обновлении ветки `master` в удаленном репозитории GitLab и выкладываются в страницы проекта по адресу: [http://msdtm.pages.bmstu.codes/labs/l1/](http://msdtm.pages.bmstu.codes/labs/l1/).

## Диаграмма последовательности

Последовательность выполнения команды load:

![Последовательность выполнения команды load](doc/load.JPG)

## Выводы

!!!!!!!!!!!
