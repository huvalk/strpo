package repository

import (
	"errors"
)

var MaxNameLength = 50
var MinYearOfBirth = uint16(1900)
var MaxYearOfBirth = uint16(2019)

type Person struct {
	ID          int64
	FirstName   string
	LastName    string
	YearOfBirth uint16
}

var ErrInvariant = errors.New("недопустимое состояние объекта")

func NewPerson(firstName, lastName string, yearOfBirth uint16) (*Person, error) {
	p := &Person{
		FirstName:   firstName,
		LastName:    lastName,
		YearOfBirth: yearOfBirth,
	}

	if !p.invariant() {
		return nil, ErrInvariant
	}
	return p, nil
}

func (r *Person) invariant() bool {
	return r.YearOfBirth >= MinYearOfBirth &&
		r.YearOfBirth <= MaxYearOfBirth &&
		!(len(r.FirstName) == 0) && len(r.FirstName) <= MaxNameLength &&
		!(len(r.LastName) == 0) && len(r.LastName) <= MaxNameLength
}

func (r *Person) IsValid() error {
	if r.invariant() {
		return nil
	}

	return ErrInvariant
}
