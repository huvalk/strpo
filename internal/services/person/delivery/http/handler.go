package http

import (
	"encoding/json"
	"errors"
	"github.com/kataras/golog"
	"github.com/labstack/echo"
	"lab1/internal/services/person/repository"
	"lab1/internal/services/person/usecase"
	"net/http"
	"strconv"
)

type PersonHandler struct {
	u *usecase.PersonUseCase
}

func NewPersonHandler(e *echo.Echo, u *usecase.PersonUseCase) *PersonHandler {
	handler := &PersonHandler{u: u}

	e.POST("/person", handler.Add)
	e.DELETE("/person/:id", handler.Remove)
	e.PUT("/person/:id", handler.Update)
	e.GET("/person", handler.View)
	e.DELETE("/person", handler.Clean)

	return handler
}

func (h *PersonHandler) Add(c echo.Context) error {
	p := &repository.Person{}
	if err := json.NewDecoder(c.Request().Body).Decode(&p); err != nil {
		golog.Errorf("Failed to decode json from request: %v", p)
		return echo.ErrBadRequest
	}

	id, err := h.u.Add(p)
	if err != nil {
		golog.Errorf("Failed to add person: %v\n%v", err, p)
		return echo.ErrInternalServerError
	}

	return c.JSON(http.StatusCreated, AddResponse{ID: id})
}

func (h *PersonHandler) Remove(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		golog.Errorf("Failed to get url variable")
		return echo.NewHTTPError(http.StatusInternalServerError, errors.New("id not found"))
	}

	err = h.u.Remove(int64(id))
	if err != nil {
		golog.Errorf("Failed to remove person: %v", err)
		return echo.ErrInternalServerError
	}

	return c.JSON(http.StatusOK, nil)
}

func (h *PersonHandler) Update(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		golog.Errorf("Failed to get url variable")
		return echo.NewHTTPError(http.StatusInternalServerError, errors.New("id not found"))
	}

	p := &repository.Person{ID: int64(id)}
	if err := json.NewDecoder(c.Request().Body).Decode(&p); err != nil {
		golog.Errorf("Failed to decode json from request: %v", p)
		return echo.ErrBadRequest
	}

	err = h.u.Update(int64(id), p)
	if err != nil {
		golog.Errorf("Failed to update person: %v", err)
		return echo.ErrInternalServerError
	}

	return c.JSON(http.StatusOK, nil)
}

func (h *PersonHandler) View(c echo.Context) error {
	persons, err := h.u.View()
	if err != nil {
		golog.Errorf("Failed to view persons", err)
		return echo.ErrInternalServerError
	}

	return c.JSON(http.StatusOK, ViewResponse{Persons: persons})
}

func (h *PersonHandler) Clean(c echo.Context) error {
	err := h.u.Clean()
	if err != nil {
		golog.Errorf("Failed to clean persons", err)
		return echo.ErrInternalServerError
	}

	return c.JSON(http.StatusOK, nil)
}
