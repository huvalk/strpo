#!/bin/bash
if ((EUID != 0)); then
  echo "Granting root privileges for script ( $SCRIPT_NAME )"
  if [[ -t 1 ]]; then
    sudo "$0" "$@"
  else
    exec 1>output_file
    gksu "$0" "$@"
  fi
  exit
fi

apt update && apt upgrade -y
wget -O /tmp/go1.17.7.linux-amd64.tar.gz https://go.dev/dl/go1.17.7.linux-amd64.tar.gz
tar -C /usr/local -xzf /tmp/go1.17.7.linux-amd64.tar.gz

echo "Done"