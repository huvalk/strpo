package sqlite

import (
	"database/sql"
	"lab1/internal/services/person/repository"
)

type PersonRepository struct {
	db *sql.DB
}

func NewPersonRepository(db *sql.DB) *PersonRepository {
	return &PersonRepository{db: db}
}

func (r *PersonRepository) Add(p *repository.Person) (int64, error) {
	req := "INSERT INTO persons (firstName, lastName, yearOfBirth) " +
		"VALUES( $1, $2, $3, $4) " +
		"RETURNING id"

	id := int64(0)
	err := r.db.QueryRow(req,
		p.FirstName, p.LastName, p.YearOfBirth).Scan(&id)
	return id, err
}

func (r *PersonRepository) Remove(id int64) error {
	req := "DELETE FROM persons " +
		"WHERE id = $1"

	_, err := r.db.Exec(req, id)
	return err
}

func (r *PersonRepository) Update(id int64, p *repository.Person) error {
	req := "UPDATE persons " +
		"SET firstName=$1, lastName=$2, yearOfBirth=$3 " +
		"WHERE id = $1"

	_, err := r.db.Exec(req, id, p.FirstName, p.LastName, p.YearOfBirth)
	return err
}

func (r *PersonRepository) View() ([]repository.Person, error) {
	req := "SELECT id, firstName, lastName, yearOfBirth " +
		"FROM persons"

	rows, err := r.db.Query(req, req)
	if err != nil {
		return nil, err
	}

	var res []repository.Person
	for rows.Next() {
		p := repository.Person{}
		err := rows.Scan(&p.ID, &p.FirstName, &p.LastName, &p.YearOfBirth)
		if err != nil {
			return nil, err
		}

		res = append(res, p)
	}
	return res, err
}

func (r *PersonRepository) Clean() error {
	req := "DELETE FROM persons"

	_, err := r.db.Exec(req)
	return err
}
